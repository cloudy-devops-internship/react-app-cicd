# Task 2.2

## Description
Simple create-react-app from open Github repo.

Source:
https://github.com/mydatahack/react-form-unit-test-example

## Prerequisites
1) Sign up on AWS if you don't have account, and create ec2 instance (eligible for free tier will be enough)
2) Install all dependencies on ec2 (e.g. nodejs), download repository, serving dependencies (to serve react app)
3) Manually open needed ports on ec2 in security group for every IP.

## Pipeline task
Create pipeline with Gitlab CI/CD on base of node.js:
1) Install npm dependencies
2) Run tests
3) Connect by ssh to ec2 instance: build binary, run react binaries